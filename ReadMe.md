# Diabetes Readmission Classifier
Thema09-1.0-SNAPSHOT-all is a jar file that will determine whether a patient will be readmitted to the hospital based on 19 distict attributes.

## Getting started
Please ensure to follow the following instructions to the letter. If the instructions are not followed the program will not produce any results.

## Prerequisites
This program can determine the readmission classes of either a single instance or an entire file, both provided through the command line.  
Copying the following line in the command line will produce a small overview on which options can be provided and what these options do.  
```bash
java -jar Thema09-1.0-SNAPSHOT-all.jar -h
```


### Providing an instance
The instance must contain 19 attributes,each attribute seperated by a comma.
Each individual attribute must be between single quotes.
Below are the attributes this program will expect. Ensure the attributes are provided in the same order, and that they do not contain a value that is not expected.
If the listed expected value is numeric, it can be any numeric value.(Within reason)  

1. race  
 * ?  
 * AfricanAmerican    
 * Asian   
 * Caucasian  
 * Hispanic  
 * Other
2. age  
 * [0-10)  
 * [10-20)  
 * [20-30)  
 * [30-40)  
 * [40-50)  
 * [50-60)  
 * [60-70)  
 * [70-80)  
 * [80-90)
 * [90-100)  
3. time_in_hospital  
 * numeric
4. medical_specialty  
 * ?  
 * AllergyandImmunology  
 * Anesthesiology  
 * Anesthesiology-Pediatric  
 * Cardiology  
 * Cardiology-Pediatric  
 * DCPTEAM  
 * Dentistry  
 * Dermatology  
 * Emergency/Trauma  
 * Endocrinology  
 * Endocrinology-Metabolism  
 * Family/GeneralPractice  
 * Family/GeneralPractice  
 * Gastroenterology  
 * Gynecology  
 * Hematology  
 * Hematology/Oncology  
 * Hospitalist  
 * InfectiousDiseases  
 * InternalMedicine  
 * Nephrology  
 * Neurology  
 * Neurophysiology  
 * Obsterics&Gynecology-GynecologicOnco  
 * Obstetrics  
 * ObstetricsandGynecology  
 * Oncology  
 * Ophthalmology  
 * Orthopedics  
 * Orthopedics-Reconstructive  
 * Osteopath  
 * Otolaryngology  
 * OutreachServices  
 * Pathology  
 * Pediatrics  
 * Pediatrics-AllergyandImmunology  
 * Pediatrics-CriticalCare  
 * Pediatrics-EmergencyMedicine  
 * Pediatrics-Endocrinology  
 * Pediatrics-Hematology-Oncology  
 * Pediatrics-InfectiousDiseases  
 * Pediatrics-Neurology  
 * Pediatrics-Pulmonology  
 * Perinatology  
 * PhysicalMedicineandRehabilitation  
 * PhysicianNotFound  
 * Podiatry  
 * Proctology  
 * Psychiatry  
 * Psychiatry-Addictive  
 * Psychiatry-Child/Adolescent  
 * Psychology  
 * Pulmonology  
 * Radiologist  
 * Radiology
 * Resident  
 * Rheumatology  
 * Speech  
 * SportsMedicine  
 * Surgeon  
 * Surgery-Cardiovascular  
 * Surgery-Cardiovascular/Thoracic  
 * Surgery-Colon&Rectal  
 * Surgery-General  
 * Surgery-Maxillofacial  
 * Surgery-Neuro  
 * Surgery-Pediatric  
 * Surgery-Plastic  
 * Surgery-PlasticwithinHeadandNeck  
 * Surgery-Thoracic  
 * Surgery-Vascular  
 * SurgicalSpecialty  
 * Urology
5. num_lab_procedures  
 * numeric
6. num_procedures  
 * numeric
7. number_outpatient  
 * numeric
8. number_emergency  
 * numeric  
9. number_inpatient  
 * numeric  
10. max_glu_serum  
 * >200  
 * >300  
 * None  
 * Norm  
11. repaglinide  
 * Down  
 * No  
 * Steady  
 * Up  
12. nateglinide  
 * Down  
 * No  
 * Steady  
 * Up  
13. glipizide  
 * Down  
 * No  
 * Steady  
 * Up  
14. glyburide  
 * Down  
 * No  
 * Steady  
 * Up  
15. pioglitazone  
 * Down  
 * No  
 * Steady  
 * Up  
16. glyburide.metformin  
 * Down  
 * No  
 * Steady  
 * Up  
17. change  
 * Ch  
 * No  
18. diabetesMed  
 * No  
 * Yes  
19. readmitted  
 * This is the class label, and the unknown attribute. This should be given as a ?.

An example of how an instance should look like can be found below:  
'?','[70-80)',5,'?',70,0,0,0,1,'None','No','No','Steady','No','No','No','Ch','Yes',?  
  
If this specific instance were to be determined it would look like this:
```bash
java -jar Thema09-1.0-SNAPSHOT-all.jar -i '?','[70-80)',5,'?',70,0,0,0,1,'None','No','No','Steady','No','No','No','Ch','Yes',?
```  
The program will write an results.arff file in the same location as where Thema09-1.0-SNAPSHOT-all.jar is located.  
If multiple files are to be used, before running the program a second time, save the first results.arff to a different name.  
This file will always only contain the results of the last provided file, and will overwrite previous results.  


### Providing a file
This program will only work when providing an .arff file. Please ensure the provided file has this extension and matches the following template:

@relation write.to.file  
@attribute race {'?','AfricanAmerican','Asian','Caucasian','Hispanic','Other'}  
@attribute age {'[0-10)','[10-20)','[20-30)','[30-40)','[40-50)','[50-60)','[60-70)','[70-80)','[80-90)','[90-100)'}  
@attribute time_in_hospital numeric  
@attribute medical_specialty {'?','AllergyandImmunology','Anesthesiology','Anesthesiology-Pediatric','Cardiology','Cardiology-Pediatric','DCPTEAM','Dentistry','Dermatology','Emergency/Trauma','Endocrinology','Endocrinology-Metabolism','Family/GeneralPractice','Gastroenterology','Gynecology','Hematology','Hematology/Oncology','Hospitalist','InfectiousDiseases','InternalMedicine','Nephrology','Neurology','Neurophysiology','Obsterics&Gynecology-GynecologicOnco','Obstetrics','ObstetricsandGynecology','Oncology','Ophthalmology','Orthopedics','Orthopedics-Reconstructive','Osteopath','Otolaryngology','OutreachServices','Pathology','Pediatrics','Pediatrics-AllergyandImmunology','Pediatrics-CriticalCare','Pediatrics-EmergencyMedicine','Pediatrics-Endocrinology','Pediatrics-Hematology-Oncology','Pediatrics-InfectiousDiseases','Pediatrics-Neurology','Pediatrics-Pulmonology','Perinatology','PhysicalMedicineandRehabilitation','PhysicianNotFound','Podiatry','Proctology','Psychiatry','Psychiatry-Addictive','Psychiatry-Child/Adolescent','Psychology','Pulmonology','Radiologist','Radiology','Resident','Rheumatology','Speech','SportsMedicine','Surgeon','Surgery-Cardiovascular','Surgery-Cardiovascular/Thoracic','Surgery-Colon&Rectal','Surgery-General','Surgery-Maxillofacial','Surgery-Neuro','Surgery-Pediatric','Surgery-Plastic','Surgery-PlasticwithinHeadandNeck','Surgery-Thoracic','Surgery-Vascular','SurgicalSpecialty','Urology'}  
@attribute num_lab_procedures numeric  
@attribute num_procedures numeric  
@attribute number_outpatient numeric  
@attribute number_emergency numeric  
@attribute number_inpatient numeric  
@attribute max_glu_serum {'>200','>300','None','Norm'}  
@attribute repaglinide {'Down','No','Steady','Up'}  
@attribute nateglinide {'Down','No','Steady','Up'}  
@attribute glipizide {'Down','No','Steady','Up'}  
@attribute glyburide {'Down','No','Steady','Up'}  
@attribute pioglitazone {'Down','No','Steady','Up'}  
@attribute glyburide.metformin {'Down','No','Steady','Up'}  
@attribute change {'Ch','No'}  
@attribute diabetesMed {'No','Yes'}  
@attribute readmitted {'NO','YES'}  
@data  

The instances can then be written under data and each single instance should be on a single line.  
The program can be started using a file in the following way:
```bash
java -jar Thema09-1.0-SNAPSHOT-all -f filepath
```
In this the filepath must be a string corresponding with the arff file containing unkown readmission classes.  
The program will write the results to results.arff, in the same way as if a single instance was provided.

# Author
* Rik van de Pol
 
