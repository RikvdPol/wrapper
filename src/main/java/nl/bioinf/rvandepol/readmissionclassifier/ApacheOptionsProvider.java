package nl.bioinf.rvandepol.readmissionclassifier;

import org.apache.commons.cli.*;
import java.io.*;

/**
 * This class is designed to process the commandline arguments.
 * It checks which option if given and then processes the given file or single instance.
 * There are three options that can be given:
 *  <p>
 *  <li> -h will print a help message and explain what to program expects
 *   <li> -f for when a file containing multiple instances should be processed
 *  <li> -i for a when only a single instance should be processed.
 *  </p>
 *
 *  <p>
 *      The help arguments will print a help message that explains which arguments can be provided
 *  </p>
 *
 *  <p>
 *      The file arguments expects a file. This program first checks if the extension of the provided file
 *      is of the .arff extension. After this is checks if the @attribute lines that are present in the arff file
 *      match the expected attribute lines. It does this by creating two array lists, on containing the expected attributes
 *      and one containing the found attributes.
 *  </p>
 *
 *  <p>
 *      The instance arguments expects a single instance string. This string is then added to
 *      a template file, and this file is processed the same as if the user provided the program with a file.
 *  </p>
 * @author rvandepol
 */

public class ApacheOptionsProvider {
    private static final String HELP = "help";
    private static final String INSTANCE = "INSTANCE";
    private static final String FILE = "FILE";

    private Options options;
    private CommandLine commandLine;

    ApacheOptionsProvider() {
        buildOptions();
    }

    /**
     * The buildOptions method determines which arguments can be parsed by the command line.
     * These options are added to an options object. The program will later check which option is provided
     * and this determines what the program will do.
     *
     */

    private void buildOptions() {
        this.options = new Options();
        Option helpOption = new Option("h", HELP, false, "This program can classify readmission rates of diabetes patients based on several attributes");
        Option instanceOption = new Option("i", INSTANCE, true, "Provide an instance");
        Option fileOption = new Option("f", FILE, true, "Provide a file");
        options.addOption(helpOption);
        options.addOption(instanceOption);
        options.addOption(fileOption);
    }

    /**
     * The processCommandLine method checks which option was provided and determines what it has to do with the provided argument.
     * If the -i and a instance string were provided it will first parse the argument to a single string.
     * After this the writeInstanceToFile method will be called. Once this is done it will provide the instance as a file to
     * processInstanceFile. Once the class of the instance has been determined the removeInstanceFromFile will be called.
     * @param args arguments provided on the command line
     */
    void processCommandLine(final String[] args) {
        try {
            CommandLineParser parser = new DefaultParser();
            this.commandLine = parser.parse(this.options, args);
            if (commandLine.hasOption(INSTANCE)) {
                String instance = this.commandLine.getOptionValue(INSTANCE);
                FileReviewer file = new FileReviewer();
                file.singleInstanceChecker(instance);

            }
            else if (commandLine.hasOption(FILE)) {
                String fileName = this.commandLine.getOptionValue(FILE);
                FileReviewer file = new FileReviewer();
                file.processInstanceFile(fileName);
            }

            else {
                printHelp();
            }
        } catch (ParseException | IOException ex) {
            throw new IllegalArgumentException(ex);
        }
    }

    /**
     * This method will print a help message. This method is called when the user provided
     * the program with -h, when an invalid option is provided or when no option at all is provided.
     */
    void printHelp() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("Determine readmission", options);
    }

}