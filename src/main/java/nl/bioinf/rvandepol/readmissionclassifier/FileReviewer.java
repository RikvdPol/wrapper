package nl.bioinf.rvandepol.readmissionclassifier;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This class receives a file from the ApacheOptionsProvider class. It will perform several checks before
 * providing it to the WekaRunner class. The class will check if the file is of the expected extension and if the
 * header equals what is expected.
 */

class FileReviewer {
    private String emptyFile = "data/temp.arff";
    String test = "build/libs/data/temp.arff";
    /**
     * This method calls the several file check methods. It will first call the hasCorrectExtension method
     * if this returns true the fileProcessor method will be called.
     * If the user provides the program with an invalid file, it will print out an error message, and
     * the program will abort processing the file.
     * @param fileName The filename as a string, provided by ApacheOptionsProvider
     */
    void processInstanceFile(String fileName) {
        try {
            System.out.println("Checking if file exists");
            if (fileExtensionChecker(fileName)) {
                System.out.println("File found");
                fileProcessor(fileName);
            } else {
                throw new IllegalArgumentException();
            }
        } catch (IllegalArgumentException illegalFile) {
            System.err.println("The file you have provided could not be found");
            System.err.println("Aborting program");
            System.exit(0);
        }
    }

    /**
     * The fileProcessor method will firstly create two Arraylists. One will contain the expected header of the arff file,
     * the other will contain the header found in the file provided by the user via the command line.
     * These Arraylists are created using the createAttributeLists method.
     * It will then check if these match, and if they do the file will be provided to the WekaRunner class.
     * If the headers do not match the program will print an error message and it will abort processing the file further.
     * @param fileName The filename provided by the user or the file created using a provided instance
     */

    private void fileProcessor(String fileName) {
        ArrayList expectedAttributes = createAttributeLists(fileName);
        ArrayList providedAttributes = createAttributeLists(emptyFile);
        if (expectedAttributes.equals(providedAttributes)) {
            System.out.println("All found attributes match expected");
            System.out.println("Running program");
            WekaRunner.startWeka(fileName);
        } else {
            System.err.println("Error detected in attributes");
            System.err.println("Aborting program");
            System.exit(0);
        }

    }

    /**
     * The fileExtensionChecker simply checks if the provided file is of the arff extension.
     * It will return either true or false.
     * @param fileName The filename provided by the user or the file created using a provided instance
     * @return boolean value
     */
    private boolean fileExtensionChecker(String fileName) {
        System.out.println("Checking file:" + fileName);
        return fileName.endsWith("arff");
    }

    /**
     * This method creates an ArrayList containing the header of the file provided to it.
     * It will add all lines starting with @attribute to an array, and returns it.
     * By providing the method with a template containing the correct header in the fileProcessor method it will always produce
     * a correct Array. The expected Array is then compared to the Array constructed from the provided file, and if these are the same
     * file processing will continue.
     * @param filePath the path containing the file of which an ArrayList containing the header has to be created
     * @return attributeList
     */

    private ArrayList<String> createAttributeLists(String filePath) {
        File inFile = new File(filePath);
        ArrayList<String> attributeList = new ArrayList<>();
        try {
            BufferedReader br;
            String sCurrentLine;

            br = new BufferedReader(new FileReader(inFile));

            while ((sCurrentLine = br.readLine()) != null) {
                if (sCurrentLine.startsWith("@attribute")) {
                    attributeList.add(sCurrentLine);
                }
            }
        } catch (IOException e) {

            e.printStackTrace();
        }
        return attributeList;
    }

    /**
     * The writeInstanceToFile method simply writes the provided instance to a file, so that the newly
     * written file can be provided to the processInstanceFile method.
     * @param instance The provided instance with an undetermined class
     */

    private void writeInstanceToFile(String instance){
        try {
            Files.write(Paths.get(emptyFile), instance.getBytes(), StandardOpenOption.APPEND);
        }catch (IOException e) {
            System.out.println("Error Found");
        }
    }

    /**
     * The removeInstanceFromFile method requires the same string provided to the writeInstanceToFile method.
     * This method serves to remove the recently added instance from the file, so that it can easily be used again
     * without removing the instance manually.
     * @param instance The provided instance with an undetermined class
     * @throws IOException Catches file object error
     */

    private void removeInstanceFromFile(String instance) throws IOException
    {
        File file = new File(emptyFile);
        List<String> out = Files.lines(file.toPath()).filter(line -> !line.contains(instance)).collect(Collectors.toList());
        Files.write(file.toPath(), out, StandardOpenOption.WRITE, StandardOpenOption.TRUNCATE_EXISTING);
    }

    /**
     * If a single instance is provided, checks if the instance contains the right number of attributes.
     * @param instance The provided instance with an undetermined class
     * @throws IOException Catches file object error
     */

    void singleInstanceChecker (String instance) throws IOException {
        int expectedAttributes = 19;
        String[] numberOfAttributes = instance.split(",");
        if(numberOfAttributes.length == expectedAttributes) {
            writeInstanceToFile(instance);
            processInstanceFile(this.emptyFile);
            removeInstanceFromFile(instance);
        }
        else {
            System.err.println("Incorrect number of attributes provided");
        }
    }
}