package nl.bioinf.rvandepol.readmissionclassifier;

/**
 * Main class designed to work with user input provided standard CL arguments and parsed using Apache CLI. Class is
 * final because it is not designed for extension.
 *
 * @author rvandepol
 */
public final class CommandLineArgsRunner {
    /**
     * Receives the command line arguments and provides them to ApacheOptionsProvider
     * @param args the command line arguments
     */
    public static void main(final String[] args) {
        System.out.println(System.getProperty("user.dir"));
        ApacheOptionsProvider apacheOptionsProvider = new ApacheOptionsProvider();
        try {
            apacheOptionsProvider.processCommandLine(args);
        } catch (IllegalArgumentException ex) {
            System.err.println("Illegal option or no argument provided");
            apacheOptionsProvider.printHelp();
        }
    }
}
