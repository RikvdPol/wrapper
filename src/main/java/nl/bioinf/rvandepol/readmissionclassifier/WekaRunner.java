package nl.bioinf.rvandepol.readmissionclassifier;

import weka.classifiers.AbstractClassifier;
import weka.classifiers.Evaluation;
import weka.classifiers.meta.CostSensitiveClassifier;
import weka.core.Instances;
import weka.core.converters.ConverterUtils;
import weka.core.converters.ConverterUtils.DataSource;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Random;

/**
 * If all checks in the FileReviewer class pass, this class will receive the file which has to be processed.
 * First a weka model is loaded, which is used to predict the class of the instances in the provided file
 */
class WekaRunner {


    static void startWeka(String fileToBeTested) {
        WekaRunner runner = new WekaRunner();
        runner.start(fileToBeTested);
    }


    /**
     * The start method will call all the necessary methods to determine the unknown classes of the provided instances.
     *
     * @param fileToBeTested The file containing the instances of which the class has to be predicted
     */
    private void start(String fileToBeTested) {
        try {
            AbstractClassifier fromFile = loadClassifier();
            Instances unknownInstances = loadArff(fileToBeTested);
            classifyNewInstance(fromFile, unknownInstances);
        } catch (Exception e) {
            System.err.println("An error occurred.");
            System.err.println("Available info: " + e.getMessage());
            System.err.println("Aborting.");
        }
    }

    /**
     * The classifyNewInstance method will classify the undetermined classes of the provided instances
     * and write them to a results file.
     *
     * @param fromFile         The pre established model created in weka.
     * @param unknownInstances The instances with undetermined classes
     * @throws Exception
     */

    private void classifyNewInstance(AbstractClassifier fromFile, Instances unknownInstances) throws Exception {
        // create copy
        Instances labeled = new Instances(unknownInstances);
        // label instances
        for (int i = 0; i < unknownInstances.numInstances(); i++) {
            double clsLabel = fromFile.classifyInstance(unknownInstances.instance(i));
            labeled.instance(i).setClassValue(clsLabel);
        }
        writeResultsToFile(labeled.toString());
    }

    /**
     * Loads the pre established model created in weka.
     *
     * @return The Simple Logistics model
     * @throws Exception Catches all possible exceptions
     */

    private CostSensitiveClassifier loadClassifier() throws Exception {
        String modelFile = "data/SimpleLModelCSFinal.model";
        return (CostSensitiveClassifier) weka.core.SerializationHelper.read(modelFile);
    }

    /**
     * Loads the arff file with instances which classes are unknown.
     *
     * @param datafile The file containing the instances of which the class has to be predicted
     * @return data Instance object containing the unknown file
     * @throws Exception Catches all possible exceptions
     */
    private Instances loadArff(String datafile) throws Exception {
        DataSource source = new DataSource(datafile);
        Instances data = source.getDataSet();
        if (data.classIndex() == -1)
            data.setClassIndex(data.numAttributes() - 1);
        return data;

    }


    /**
     * The writeResultsToFile method writes the results produced by weka to a file. It first deletes all content in
     * the file, so that it is empty when writing new data to the file.
     *
     * @param newLabeled The results produced by weka.
     */

    private void writeResultsToFile(String newLabeled) {
        String resultsFile = "data/results.arff";
        try {
            new FileOutputStream(resultsFile).close();
            Files.write(Paths.get(resultsFile), newLabeled.getBytes(), StandardOpenOption.APPEND);
            System.out.println("Results written to: " + resultsFile);
            Evaluate();
        } catch (Exception e) {
            System.err.println("Error detected");
        }
    }

    /**
     * The evaluate method will print the model statistics to the terminal.
     * @throws Exception catches all possible exceptions
     */

    private void Evaluate() throws Exception {
            ConverterUtils.DataSource source = new ConverterUtils.DataSource("data/preWekaNotDummy.arff");
            Instances dataSet = source.getDataSet();
            dataSet.setClassIndex(dataSet.numAttributes() - 1);
            CostSensitiveClassifier slm = loadClassifier();
            Evaluation eval = new Evaluation(dataSet);
            Random rand = new Random(1);
            int folds = 10;
            eval.crossValidateModel(slm, dataSet, folds, rand);
            System.out.println(eval.toSummaryString("Evaluation results:\n", false));
            System.out.println(eval.toMatrixString("=== Overall Confusion Matrix ===\n"));

        }
    }


