/*
 * Copyright (c) 2015 Michiel Noback
 * All rights reserved
 * www.bioinf.nl, www.cellingo.net
 */
package nl.bioinf.rvandepol.nomivoorbeelden;

/**
 * This enum specifies the possible verbosity levels used in the application.
 * @author michiel
 */
public enum VerbosityLevel {
    /**
     * basic geek level.
     */
    STRONG_SILENT_TYPE,
    /**
     * an average persons' talkativity.
     */
    NORMAL,
    /**
     * very verbose indeed.
     */
    YOU_TALK_TOO_MUCH;
}
